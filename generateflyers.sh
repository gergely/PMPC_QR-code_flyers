# !/bin/bash

# generating QR codes, tex, and pdf files for everly language in llist.txt from file from template.tex and PMPC_sticker_v2_55x75.pdf

if [ ! -f PMPC_sticker_v2_55x75.pdf ];  then
    echo "File PMPC_sticker_v2_55x75.pdf does not exist."
    exit 1
else 
    command -v qrencode >/dev/null 2>&1  || { echo "Command qrencode isn't istalled."; exit 2 ;}
    command -v pdflatex >/dev/null 2>&1  || { echo "Command pdflatex isn't istalled."; exit 3 ;}
    command -v pdfnup >/dev/null 2>&1  || { echo "Command pdfnup isn't istalled."; exit 4 ;}
    for i in $(cat llist.txt); do 
       	qrencode "https://publiccode.eu/$i/" -o qrcodes/QRcode-$i.png; 
	sed -e "s/-en/-$i/" -e "s/\/en\//\/$i\//" template.tex > flyer_$i.tex  
	pdflatex flyer_$i.tex
	pdfnup --nup 2x1 --landscape --suffix '2in1' flyer_$i.pdf flyer_$i.pdf 
	pdfnup --nup 2x2 --frame true --no-landscape --suffix '4in1' flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf
	pdfnup --nup 4x2 --frame true  --landscape --suffix '8in1' flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf flyer_$i.pdf 
    done
fi 

