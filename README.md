## pdf flyers for the PMPC campaign containing QR-codes

The idea is that people using mobile devices may be too lazy to type "publiccode.eu" to open the site but would happily visit the site by scanning a QR code. 

Another point is that it is easier and cheaper to reproduce these flyers locally than the stickers. Anyone can spread the message on bulletin board and public spaces just by printing a couple of these at home. 

```generateflyers.sh``` is a simple bash script creating pdf flyers for the [PMPC](https://publiccode.eu) campaign containing QR-codes of the sites of the corresponding languages. It generates the files from the [sticker](http://download.fsfe.org/advocacy/promomaterial/PMPC/PMPC_sticker_v2_55x75.pdf) and QR-codes of the sites of the appropriate languages created by qrencode.

Please, use it, remix it, and improve it as you wish.  

